from django.conf.urls import url
from . import views
from django.contrib.auth.decorators import login_required

app_name = 'api'
urlpatterns = [
    url(r'^test', login_required(views.test), name="test"),
    url(r'^list/$', login_required(views.listing), name="list"),
    url(r'^main/$', login_required(views.MainListView.as_view()), name="main"),
    url(r'^department/(?P<dep_id>[0-9]+)$', login_required(views.DepartmentView.as_view()), name="department"),
    url(r'^contact/(?P<pk>[0-9]+)$', login_required(views.ContactView.as_view()), name="contact"),
    url(r'^search/', login_required(views.ContactSearch.as_view()), name="search"),
    url(r'^admin/download/', login_required(views.download_csv), name="download"),
    url(r'^push', login_required(views.send_push_notification), name="push"),
    url(r'^$', views.LoginView.as_view(), name="login"),
    url(r'^logout/$', login_required(views.logout_view), name="logout"),
]
