# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-09-30 09:53
from __future__ import unicode_literals

import api.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0008_auto_20170930_0948'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='docfile',
            field=models.FileField(upload_to='', validators=[api.validators.validate_file_extension]),
        ),
    ]
