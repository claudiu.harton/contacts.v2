# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-09-30 08:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0004_merge_20170929_2106'),
    ]

    operations = [
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('docfile', models.FileField(upload_to='')),
            ],
        ),
        migrations.AlterField(
            model_name='contact',
            name='status',
            field=models.CharField(blank=True, choices=[('A', 'ACTIV'), ('I', 'INACTIV'), ('L', 'ALUMNUS')], max_length=1, null=True),
        ),
    ]
