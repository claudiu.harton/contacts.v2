# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-28 09:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_auto_20170528_1324'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contact',
            name='status',
            field=models.CharField(choices=[('A', 'ACTIV'), ('I', 'INACTIV'), ('L', 'ALUMNUS')], max_length=1),
        ),
    ]
