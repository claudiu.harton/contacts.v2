from django.db import models
from django.core.validators import FileExtensionValidator
import os


class Department(models.Model):
    name = models.CharField(max_length=30)
    short_name = models.CharField(max_length=5)

    def __str__(self):
        return self.short_name


class Contact(models.Model):

    STATUS_CHOICES = (
        ('M', 'Membru'),
        ('L', 'Lead divizie'),
        ('A', 'Alumnus'),
    )

    last_name = models.CharField(max_length=50)
    first_name = models.CharField(max_length=50)
    email = models.EmailField(max_length=255)
    phone = models.CharField(max_length=10, default="-")
    facebook = models.URLField(max_length=255, default="-")
    department = models.ForeignKey(Department,on_delete=models.CASCADE)
    divizie = models.CharField(max_length=40, null=True)
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, default='M')

    def __str__(self):
        return '{} {}'.format(self.last_name, self.first_name)

    def __unicode__(self):
        return u'%s %s' % (self.first_name, self.last_name)


class Document(models.Model):
    docfile = models.FileField(upload_to="CSV", blank=True, default=None, null=True, validators=[FileExtensionValidator(["csv"])])
    uploader = models.CharField(max_length=255, blank=True, default=None, null=True)
    time = models.CharField(max_length=255, blank=True, default=None, null=True)

    def __str__(self):
        return self.docfile.name

    def delete(self, *args, **kwargs):  ##doesnt work for bulk delete
        if os.path.isfile(self.docfile.path):
            os.remove(self.docfile.path)
        super(Document,self).delete(*args,**kwargs)