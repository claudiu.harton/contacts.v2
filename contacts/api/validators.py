from django.core.exceptions import ValidationError

def validate_file_extension(value):
    if value.file.content_type != 'text/csv':
        raise ValidationError('Error message')
