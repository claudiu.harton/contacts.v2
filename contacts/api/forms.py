from django import forms
from .models import Contact
from django.forms import ModelForm


def validate_file_extension(value):
    if not value.name.endswith('.csv'): ##must check for mime type but got an error last time when tried
        raise forms.ValidationError("Only CSV file is accepted")


class DocumentForm(forms.Form):
    docfile = forms.FileField(label='Select a file', validators=[validate_file_extension])


class NotificationForm(forms.Form):

    category_choices = [
        ("All", "All"),
        ("BE-BC", "BE-BC"),
        ("EDU", "EDU"),
        ("FR", "FR"),
        ("HR", "HR"),
        ("IP", "IP"),
        ("IT", "IT")
    ]

    title = forms.CharField(required=True)
    body = forms.CharField(widget=forms.Textarea, required=True)
    department = forms.ChoiceField(choices=category_choices, required=True)


class UpdateContactForm(ModelForm):
    class Meta:
        model = Contact
        fields = ['phone']