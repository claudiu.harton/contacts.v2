import os
import datetime
import csv
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.views import View
from django.contrib.auth.models import User
from django.contrib.auth import views
from django.views.decorators.http import require_POST
from django.views.generic import DetailView
from django.urls import reverse, reverse_lazy
from django.shortcuts import render, redirect
from django.views.generic import ListView, TemplateView
from .models import Department, Contact, Document
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from . import models
from .forms import DocumentForm, NotificationForm
from webpush import send_group_notification, send_user_notification


class LoginView(TemplateView):
    template_name = 'hello.html'

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            return redirect('api:main')
        return super().dispatch(request, *args, **kwargs)


def logout_view(request):
    template_response = views.logout_then_login(request, login_url=reverse_lazy('api:login'))
    return template_response


def download_csv(request):
    if request.user.is_staff:
        nume_c = Document.objects.all()[0]
        with open(nume_c.docfile.path, 'r', encoding='utf-8') as csvfile:
            response=HttpResponse(csvfile.read())
            response['content_type']='text/csv'
            response['Content-Disposition']='attachment;filename=contacts.csv'
            return response
    else:
        HttpResponseRedirect(reverse_lazy('api:main'))


class ContactSearch(ListView):
    queryset = models.Contact.objects.all()
    template_name = "search.html"
    context_object_name = "contacts"

    def get_context_data(self,**kwargs):
        context = super(ContactSearch, self).get_context_data(**kwargs)
        try:
            context['current_user'] = Contact.objects.values_list('pk', flat=True).filter(email=self.request.user.email).get()
        except:
            pass
        context['contacts_count'] = Contact.objects.all().count()
        return context


def test(request):
    nume_c = Document.objects.all()[0]
    with open(nume_c.docfile.path, 'r', encoding='utf-8') as csvfile:
        reader = csv.DictReader(csvfile,
                                fieldnames=['last_name', 'first_name', 'email', 'phone', 'status', 'facebook', 'department', 'divizie'])
        for row in reader:
            department = models.Department.objects.filter(short_name=row['department']).get()
            existing_contact = models.Contact.objects.filter(email=row['email']).all()

            with open(os.path.join(os.path.dirname(__file__), 'emails.txt'),'a') as file:
                file.write(row['email'] + '\n')

            if existing_contact.exists():
                contact = existing_contact[0]
                contact.last_name = row['last_name']
                contact.first_name = row['first_name']
                contact.phone = row['phone']
                contact.facebook = row['facebook']
                contact.department = department
                contact.divizie = row['divizie']
                contact.status = row['status']
                contact.save()
            else:
                models.Contact(last_name=row['last_name'],
                               first_name=row['first_name'],
                               email=row['email'],
                               phone=row['phone'],
                               facebook=row['facebook'],
                               department=department,
                               divizie = row['divizie'],
                               status = row['status']).save()
        return HttpResponseRedirect(reverse('api:main'))


def listing(request):
    documents = Document.objects.all()

    if request.method == 'GET':
        if not request.user.is_staff:
            return HttpResponseRedirect(reverse('api:main'))

    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            for i in range(0, Document.objects.filter().count()):
                Document.objects.all()[i].delete()

            newdoc = models.Document(docfile = request.FILES['docfile'], uploader = str(request.user.email).partition("@")[0], time = datetime.datetime.now().strftime('%H:%M %d/%m/%Y'))
            newdoc.docfile.name = "contacts.csv"
            newdoc.save()
            messages.success(request, 'Form submission successful')
        else:
            return render(request, 'list.html', {
                'documents': documents,
                'form': form,
            }, content_type='text/html')

        return HttpResponseRedirect(reverse('api:list'))

    else:
        userEmail = request.user.email
        try:
            user_id = Contact.objects.values_list('pk', flat=True).filter(email=userEmail).get()
        except:
            user_id = 0

        form = DocumentForm()
        notification_form = NotificationForm()
        return render(request,'list.html', {
            'documents': documents, 'form': form, 'notification_form': notification_form, 'cUser': user_id,
        }, content_type='text/html')


class MainListView(ListView):
    template_name = "main.html"
    model = Department
    context_object_name = "departments"

    def get_context_data(self, **kwargs):
        context = super(MainListView, self).get_context_data(**kwargs)
        try:
            context['contact'] = Contact.objects.filter(email=self.request.user.email).get()
        except:
            pass

        return context


class DepartmentView(View):
    model = Contact
    context_object_name = 'contacts'
    template_name = 'department_list.html'

    def get(self,request,dep_id):
        name = Department.objects.get(pk=int(dep_id)).short_name
        count = Contact.objects.filter(department=int(dep_id)).count()
        contacts = Contact.objects.filter(department=int(dep_id))
        try:
            cUser = Contact.objects.values_list('pk', flat=True).filter(
                email=self.request.user.email).get()
        except:
            cUser = 0
        return render(request, 'department_list.html', {
            'dep_id': int(dep_id),
            'contacts': contacts,
            'dep_name': name,
            'dep_count': count,
            'cUser' : cUser
        }, content_type='text/html')


class ContactView(SuccessMessageMixin,DetailView):
    model = Contact
    context_object_name = "contact"
    template_name = 'contact.html'

    def get(self,request,*args,**kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        context['department_id'] = Department.objects.values_list('pk', flat=True).filter(
            short_name=context['contact'].department)
        context['webpush'] = {"group": context['contact'].department}
        try:
            context['current'] = Contact.objects.values_list('pk', flat=True).filter(
                email=self.request.user.email).get()
        except:
            pass
        return self.render_to_response(context)


@require_POST
def send_push_notification(request):
    form = NotificationForm(request.POST)

    group = form['department'].value()

    payload = {"head": form['title'].value(), "body": form['body'].value(),
               "icon": "https://contacts.sisc.ro/static/images/sisc_notification.png"}

    if form.is_valid():
        if group == 'All':
            users = User.objects.all()
            for user in users:
                send_user_notification(user=user, payload=payload, ttl=1100)
        else:
            send_group_notification(group_name=form['department'].value(), payload=payload, ttl=1100)

    return HttpResponseRedirect(reverse('api:main'))
