/**
 * Created by alex on 05.05.2018.
 */

function copyToClipboard(element){

    var toCopy = document.getElementById(element + "1");

    toCopy.value = document.getElementById(element).innerText;

    toCopy.style.display = "block";

    toCopy.select();

    document.execCommand("Copy");

    toCopy.style.display = "none";

    toastr.remove();
    toastr.success(element + " was copied to clipboard");
}

function copyToClipboardSearch(element){

    var toCopy = document.getElementById("phone1");

    toCopy.value = document.getElementById(element).innerText;

    toCopy.style.display = "block";

    toCopy.select();

    document.execCommand("Copy");

    toCopy.style.display = "none";

    toastr.remove();
    toastr.success("phone was copied to clipboard");
}

$(document).ready(function () {

    $("#container").fadeIn(1500);
});
