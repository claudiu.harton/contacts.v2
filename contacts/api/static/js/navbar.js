
function openNav() {
    if(document.getElementById("mySidenav").style.width == "") {
        document.getElementById("mySidenav").style.width = "250px";
        document.getElementById("main").style.marginLeft = "250px";
        document.getElementById("title").style.display = "none";

    } else {
        closeNav();
    }

}

function closeNav() {
    document.getElementById("mySidenav").style.width = "";
    document.getElementById("main").style.marginLeft= "";
    displayTitle();
}

function displayTitle(){
    setTimeout(function(){
        document.getElementById("title").style.display = "block";
    },450);
}

$(document).ready(function(){
	$('#nav-icon1').click(function(){
		$(this).toggleClass('open');
	});
});

window.onscroll = function() {myFunction()};

var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}
