#API

System prerequisites
--------------------
*Install Python 3.x*

*Create local environment*
	python -m venv sandbox --clear
	
*Activate the sandbox*	
	source sandbox/bin/activate

*Install requirements*
	pip install -r requirements.txt
	
*Create database*
    create a local database called "django_contacts"
	
*Migrate changes*
	python manage.py migrate

*Create admin user*
	python manage.py createsuperuser
	
*Populate department tables*
    import "db.sql" in mysql (or phpmyadmin)
	
*Add your email address*
    in emails.txt from contacts/contacts/contacts

*Modify web-push library*
    copy webpush.js to "sandbox/lib/python3.7/site-packages/webpush/static/webpush/"

*Run local server (localhost:8000)*
	python manage.py runserver

*Set admin access to your email*
    in django admin panel at "/sisc-contacts-admin-interface"
    log in with the account created above in the cli
    give super user privilege to the user associated with your mail account in Users table 

*Import contacts*
    login in app on web, go to the upload page
    upload "contacts.csv"
    update database