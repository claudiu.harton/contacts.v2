USE django_contacts;
INSERT INTO `api_department` (`id`, `name`, `short_name`) VALUES
(1, 'IT', 'IT'),
(2, 'Fundraising', 'FR'),
(3, 'Human Resources', 'HR'),
(4, 'I&P', 'I&P'),
(5, 'Educational', 'EDU'),
(6, 'BE-BC', 'BE-BC'); 